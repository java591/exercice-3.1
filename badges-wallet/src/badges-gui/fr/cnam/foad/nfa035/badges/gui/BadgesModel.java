package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class BadgesModel<T> extends AbstractTableModel {

    private List<DigitalBadge> badges;
    private final String[]entetes={ "BadgeID", "Serial", "Début", "Fin", "Taille(octets)" };

    private long serialUID;

    public BadgesModel(){}
    public List getBadges(){
        return this.badges;
    }

    @Override
    public int getRowCount() {
        return badges.size();
    }

    @Override
    public int getColumnCount() {
        return entetes.length;
    }
    @Override
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return null;
    }
}
