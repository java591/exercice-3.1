package fr.cnam.foad.nfa035.badges.gui;

import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public class BadgeSizeCellRenderer {

    private Color originBackgroundColor;
    private Color originForegroundColor;

    public BadgeSizeCellRenderer(){}

    public Color getTableCellRendererComponent(){
        return null;
    }
    private String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " o";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %co", value / 1024.0, ci.current());
    }

}
